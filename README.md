<img src="https://www.datocms-assets.com/2885/1506457071-blog-terraform-list.svg" align="right" width="120">

# Test Terraform
--------------------------

## Overview
This repository contains the terraform code that comprises the infrastructure of
the TEST application on the AWS Direct platform. In this README you can find or
be directed to all the relevant information regarding the infrastructure for the
CLAIMS application.

## Requirements
Type | Version
----------- | -----------
Terraform | 0.10.x
Provider | 1.6