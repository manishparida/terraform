output "sns_topics_info" {
  value = "${aws_sns_topic.topics.*.arn[0]}"
}

output "sns_topics_alert" {
  value = "${aws_sns_topic.topics.*.arn[1]}"
}