terraform {
  backend "s3" {}
}

provider "aws" {
  version     = "~> 2.7"
  region      = "${var.aws_region}"
  assume_role {
    role_arn  = "arn:aws:iam::${var.aws_account}:role/${var.team_deployer_role}"
  }
}

resource "aws_sns_topic" "topics" {
  count               = "${length(var.sns_topics)}"
  name                = "${lookup(var.sns_topics[count.index],"name")}"
  display_name        = "${lookup(var.sns_topics[count.index],"name")}"
}

resource "aws_sns_topic_policy" "sns_topic_policy" {
  count               = "${length(var.sns_topics)}"
  arn                 = "${aws_sns_topic.topics.*.arn[count.index]}"
  policy              = "${data.aws_iam_policy_document.sns-topic-policy.*.json[count.index]}"
}

data "aws_iam_policy_document" "sns-topic-policy" {
  count               = "${length(var.sns_topics)}"
  policy_id = "__default_policy_ID"
  statement {
    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:AddPermission",
    ]
    condition {
    test    = "StringEquals"
    variable  = "AWS:SourceOwner"
    values = [
      "${var.aws_account}",
    ]
    }
    effect = "Allow"
    principals {
    type = "AWS"
    identifiers = [
      "*"
    ]
    }
    resources = [
    "${aws_sns_topic.topics.*.arn[count.index]}",
    ]
    sid = "__default_statement_ID"
  }
}

data "template_file" "sns_subscription_template" {
  count              = "${length(var.sns_topics)}"
  template           = "${file("${path.module}/sns_subscription.tpl")}"
  vars = {
    topic_name     = "${lookup(var.sns_topics[count.index],"name")}"
    endpoint       = "${lookup(var.sns_topics[count.index],"endpoint")}"
    protocol       = "${lookup(var.sns_topics[count.index],"protocol")}"
    topic_arn      = "${aws_sns_topic.topics.*.id[count.index]}"
  }
}

resource "aws_cloudformation_stack" "sns_subscription" {
  count              = "${length(var.sns_topics)}"
  name               = "${lookup(var.sns_topics[count.index],"name")}-${lookup(var.sns_topics[count.index],"protocol")}-subscription"
  template_body      = "${data.template_file.sns_subscription_template.*.rendered[count.index]}"
}

