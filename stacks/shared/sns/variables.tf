variable "team_name"            {}
variable "environment"          {}
variable "state_prefix"         {}
variable "aws_region"           {}
variable "team_deployer_role"   {}
variable "aws_account"          {}
variable "account_type"         {}
variable "sns_topics"           {
  type      = "list"
}
