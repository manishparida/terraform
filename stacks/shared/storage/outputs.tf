output "main_bucket_id" {
  value = "${aws_s3_bucket.main_s3_bucket.bucket_id}"
}

output "main_bucket_arn" {
  value = "${aws_s3_bucket.main_s3_bucket.bucket_arn}"
}
