terraform {
  backend "s3" {}
}

provider "aws" {
  version = "~> 1.0"
  region = "${var.aws_region}"
  assume_role {
    role_arn = "arn:aws:iam::${var.aws_account}:role/${var.team_name}/${var.team_deployer_role}"
  }
}

resource "aws_s3_bucket" "main_s3_bucket" {
  bucket	                  = "${var.main_bucket_name}"
  acl                         = "${var.main_bucket_acl}"
  region                      = "${var.aws_region}"
        server_side_encryption_configuration {
		rule {
			apply_server_side_encryption_by_default {
				sse_algorithm     = "AES256"
			}
		}
    }
}
