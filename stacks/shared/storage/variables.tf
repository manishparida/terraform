variable "aws_region"         {}
variable "aws_account"        {}
variable "team_name"          {}
variable "team_deployer_role" {}
variable "main_bucket_name"   {}
variable "main_bucket_acl"    {}