terraform {
  backend "s3" {}
}

provider "aws" {
  version = "~> 1.0"
  region  = "${var.aws_region}"
  assume_role {
    role_arn = "arn:aws:iam::${var.aws_account}:role/${var.team_name}/${var.team_deployer_role}"
  }
}

data "aws_vpc" "client" {
  filter {
    name                  = "tag:Name"
    values                = ["${var.vpc_name}"]
  }
  state                   = "available"
}

#---------------------ALB SG-------------------- 
module "alb_sg" {
  source = "git::https://stash.aviva.co.uk/scm/github_terraform_aws_modules/terraform-aws-security-group.git/?ref=v2.0.0"
  name                    = "${var.stack_name_app}-alb-${var.environment}"
  description             = "Security group with required app connection ports"
  vpc_id                  = "${data.aws_vpc.client.id}"
  ingress_cidr_blocks	  = ["${var.source_cidr_ingress}"]
  ingress_rules		      = ["${var.ingress_rules_list}"]
   computed_ingress_with_cidr_blocks = [
     {
      from_port 					= "${var.port_8000}"
      to_port 						= "${var.port_8000}"
      protocol 						= "${var.protocol}"
      description 					= "Ingress rule from splunk"
      cidr_blocks 					= "${var.splunk_cidr}"
    },
     {
      from_port 					= "${var.port_9997}"
      to_port 						= "${var.port_9997}"
      protocol 						= "${var.protocol}"
      description 					= "Ingress rule from splunk"
      cidr_blocks 					= "${var.splunk_cidr}"
    },
     {
      from_port 					= "${var.port_8089}"
      to_port 						= "${var.port_8089}"
      protocol 						= "${var.protocol}"
      description 					= "Ingress rule from splunk"
      cidr_blocks 					= "${var.splunk_8089_cidr}"
    },
	 {
      from_port 					= "${var.port_https}"
      to_port 						= "${var.port_https}"
      protocol 						= "${var.protocol}"
      description 					= "Ingress rule from splunk"
      cidr_blocks 					= "${var.splunk_cidr}"
    }
	
	]
    number_of_computed_ingress_with_cidr_blocks = 4
  ingress_with_cidr_blocks = []
  ingress_ipv6_cidr_blocks = []		#keep value empty if IPV6 is not using
  computed_egress_with_source_security_group_id = [
    {
      rule						= "${var.egress_rules_all}"
      source_security_group_id	= "${module.app_sg.this_security_group_id}"
    }
  ]
  number_of_computed_egress_with_source_security_group_id = 1
  egress_ipv6_cidr_blocks  = []	#keep value empty if IPV6 is not using
  tags = {
    Team                   = "${var.tag_team}"
    HSN                    = "${var.tag_hsn}"
    Costcentre_Projectcode = "${var.tag_costcentre_projectcode}"
    Owner                  = "${var.tag_owner}"
    Environment            = "${var.environment}"
  }
}

#---------------------APP SG-------------------- 
module "app_sg" {
  source = "git::https://stash.aviva.co.uk/scm/github_terraform_aws_modules/terraform-aws-security-group.git/?ref=v2.0.0"

  name                    = "${var.stack_name_app}-asg-${var.environment}"
  description             = "Security group with required app connection ports"
  vpc_id                  = "${data.aws_vpc.client.id}"
  
  computed_ingress_with_source_security_group_id = [
    {
	 from_port   = "${var.port_http}"
     to_port     = "${var.port_http}"
     protocol    = "${var.protocol}"
     description = "Ingress rule for application port 80 for web layer"
     source_security_group_id = "${module.alb_sg.this_security_group_id}"
    },
	{
	 from_port   = "${var.port_https}"
     to_port     = "${var.port_https}"
     protocol    = "${var.protocol}"
     description = "Ingress rule for application port 443 for web layer"
     source_security_group_id = "${module.alb_sg.this_security_group_id}"
    }
  ]
  number_of_computed_ingress_with_source_security_group_id = 2
  
  computed_ingress_with_cidr_blocks = [
    {
      from_port 					= "${var.port_rdp}"
      to_port 						= "${var.port_rdp}"
      protocol 						= "${var.protocol}"
      description 					= "Ingress rule for application port 3389 for web layer"
      cidr_blocks 					= "${var.source_cidr_rdp}"
    },
	{
      from_port 					= "${var.port_http}"
      to_port 						= "${var.port_http}"
      protocol 						= "${var.protocol}"
      description 					= "Ingress rule for application port 80 for web layer"
      cidr_blocks 					= "${var.source_cidr_rdp}"
    },
    {
      from_port                      = "${var.port_smb}"
      to_port                        = "${var.port_smb}"
      protocol                       = "${var.protocol}"
      description                    = "Ingress rule for application port 445 for web layer"
      cidr_blocks 					= "${var.default_cidr}"
    }
	]
    number_of_computed_ingress_with_cidr_blocks = 3
  
  #ingress_with_cidr_blocks = []
  ingress_ipv6_cidr_blocks = []		#keep value empty if IPV6 is not using
  #egress_cidr_blocks	   = ["${var.source_cidr_egress}"]
  #egress_rules			   = ["${var.egress_rules_app}"]
  
   computed_egress_with_cidr_blocks = [
    {
	 from_port                         = "${var.port_1}"
     to_port                           = "${var.port_1}"
     protocol                          = "${var.protocol}"
     description                       = "${var.description}${var.port_1}"
     cidr_blocks                       = "${var.source_cidr_egress_1}"
    },
     {
	 from_port                         = "${var.port_8000}"
     to_port                           = "${var.port_8000}"
     protocol                          = "${var.protocol}"
     description                       = "${var.description}${var.port_8000}"
     cidr_blocks                       = "${var.splunk_cidr}"
    },
    {
	 from_port                         = "${var.port_8089}"
     to_port                           = "${var.port_8089}"
     protocol                          = "${var.protocol}"
     description                       = "${var.description}${var.port_8089}"
     cidr_blocks                       = "${var.splunk_cidr}"
    },
    {
	 from_port                         = "${var.port_9997}"
     to_port                           = "${var.port_9997}"
     protocol                          = "${var.protocol}"
     description                       = "${var.description}${var.port_9997}"
     cidr_blocks                       = "${var.splunk_cidr}"
    },
     {
	 from_port                         = "${var.port_443}"
     to_port                           = "${var.port_443}"
     protocol                          = "${var.protocol}"
     description                       = "${var.description}${var.port_443}"
     cidr_blocks                       = "${var.default_cidr}"
    },
     {
	 from_port                         = "${var.port_80}"
     to_port                           = "${var.port_80}"
     protocol                          = "${var.protocol}"
     description                       = "${var.description}${var.port_80}"
     cidr_blocks                       = "${var.default_cidr}"
    }
	
	]
  number_of_computed_egress_with_cidr_blocks = 6
  egress_ipv6_cidr_blocks  = []	#keep value empty if IPV6 is not using
  tags = {
    Team                   = "${var.tag_team}"
    HSN                    = "${var.tag_hsn}"
    Costcentre_Projectcode = "${var.tag_costcentre_projectcode}"
    Owner                  = "${var.tag_owner}"
    Environment            = "${var.environment}"
  }
}
