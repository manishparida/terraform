terraform {
    backend "s3" {}
}

provider "aws" {
    version         = "~> 2.7"
    region          = "${var.aws_region}"
    assume_role {
        role_arn = "arn:aws:iam::${var.aws_account}:role/${var.team_deployer_role}"
    }
}

data "aws_vpc" "client_vpc" {
    filter {
        name    = "tag:Name"
        values     = ["${var.vpc_name}"]
    }
    state         = "available"
}

data "aws_subnet" "amber1" {
  filter {
    name   = "tag:Name"
    values = ["${var.amber_a}"]
  }
}

data "aws_subnet" "amber2" {
  filter {
    name   = "tag:Name"
    values = ["${var.amber_b}"]
  }
}
data "aws_subnet" "amber3" {
  filter {
    name   = "tag:Name"
    values = ["${var.amber_c}"]
  }
}

# APPLICATION LOAD BALANCER

resource "aws_alb" "app_alb" {
 name            = "${var.app_stack_name}-ALB-${var.tag_environment}"
 security_groups = ["sg-0be124a785749237a"]
 subnets         = ["${data.aws_subnet.amber1.id}","${data.aws_subnet.amber2.id}","${data.aws_subnet.amber3.id}"]
 internal        = "${var.alb_is_internal}"

 tags = {
 Costcentre_Projectcode         = "${var.tag_costcentre_projectcode}"
 Owner                          = "${var.tag_owner}"
 Team                           = "${var.tag_team}"
 Environment                    = "${var.environment}"
    }
}

# TARGET GROUPS

resource "aws_alb_target_group" "app_tg_http" {
	name                 		= "${var.stack_name_app}-ALB-TG-${var.service_name_http}-${var.tag_environment}"
	port                 		= "${var.tg_port_http}"          #8010
	protocol             		= "${var.tg_protocol_http}"      #"TCP"
	vpc_id               		= "${data.aws_vpc.client_vpc.id}"
	deregistration_delay 		= "${var.tg_deregistration_delay}"
	
	stickiness {
		type             		= "lb_cookie"
		cookie_duration  		= "${var.tg_cookie_duration}"
		enabled          		= "${var.tg_enable_stickiness}"
	}
	health_check {
		interval            	= "${var.tg_check_interval}"
		path                	= "${var.tg_check_path}"
		timeout             	= "${var.tg_check_timeout}"
		healthy_threshold   	= "${var.tg_healthy_threshold}"
		unhealthy_threshold 	= "${var.tg_unhealthy_threshold}"
		port                	= "${var.tg_port_http}"
		protocol            	= "${var.tg_protocol_http}"
		matcher					      = "${var.tg_matcher}"
	}
	
	tags = {
		name                    = "${var.stack_name_app}-ALB-TG-${var.service_name_http}-${var.tag_environment}"
		Costcentre_Projectcode  = "${var.tag_costcentre_projectcode}"
		tag_environment         = "${var.tag_environment}"
		Owner                   = "${var.tag_owner}"
		team                    = "${var.tag_team}"
	}
}

# APPLICATION LOAD BALANCER LISTENER

resource "aws_lb_listener" "app_alb_listener" {
 load_balancer_arn = "${aws_alb.app_alb.arn}"
 port              = "${var.instance_port}"
 protocol          = "${var.lb_protocol}"

 default_action {
   type             = "forward"
   target_group_arn = "${aws_alb_target_group.app_tg_http.arn}"
 }
}

# APPLICATION LOAD BALANCER LISTENER RULES

resource "aws_alb_listener_rule" "app_alb_listener_rule_http" {

     priority            = "${var.priority}"
     listener_arn        = "${aws_lb_listener.app_alb_listener.arn}"

     action {
       type              = "forward"
       target_group_arn  = "${aws_alb_target_group.app_tg_http.arn}"
     }

     condition {
       field             = "${var.listener_rule_field}"
       values            = ["${var.listener_rule_values}"]
     }
   }

   #___________________________________userdata___________________________________________________
data "template_file" "user_data" {
  template = "${file("user_data.tpl")}"
}

#___________________________________ASG___________________________________________________
#######################
# Launch configuration
#######################
resource "aws_launch_configuration" "app_lc" {

   name_prefix              = "${var.app_stack_name}-lc-${var.tag_environment}"
   # image_id                 = "${data.aws_ami.win2016.id}"
   image_id                 = "ami-07cd5239368f4714d"
   instance_type            = "${var.app_instance_type}"
   security_groups          = ["sg-06e56a0543f23c26d"]        #["${var.security_groups_ec2_id}","${var.mgmt_sg_id}"]
   iam_instance_profile     = "${var.app_profile_name}"
   user_data                = "${data.template_file.user_data.rendered}"
   lifecycle {
    create_before_destroy = true
  }
}

####################
# Autoscaling group
####################
resource "aws_autoscaling_group" "app_asg" {
  name 						          = "${var.app_stack_name}-asg-${var.tag_environment}"
  vpc_zone_identifier 		  = ["${data.aws_subnet.amber1.id}","${data.aws_subnet.amber2.id}","${data.aws_subnet.amber3.id}"]
  launch_configuration 	  	= "${aws_launch_configuration.app_lc.id}"
  target_group_arns 		    = ["${aws_alb_target_group.app_tg_http.arn}"]
  max_size 					        = "${var.max_size}"
  min_size 					        = "${var.min_size}"
  health_check_grace_period = "${var.health_check_grace_period}"
  health_check_type 		    = "${var.health_check_type}"
  enabled_metrics 			    =  ["GroupMinSize", "GroupMaxSize", "GroupDesiredCapacity", "GroupInServiceInstances", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]
  lifecycle {
    create_before_destroy 	= true
  }
  tag {
    key = "Costcentre_Projectcode"
    value = "${var.tag_costcentre_projectcode}"
    propagate_at_launch = true
  }
  tag {
    key = "Name"
    value = "${var.app_stack_name}-${var.tag_environment}"
    propagate_at_launch = true
 }
 tag {
   key = "Owner"
   value = "${var.tag_owner}"
   propagate_at_launch = true
 }
 tag {
   key = "role"
   value = "${var.tag_role}"
   propagate_at_launch = true
 }
 tag {
   key =  "Environment"
   value = "${var.tag_environment}"
   propagate_at_launch = true
 }
 tag {
   key =  "team"
   value = "${var.tag_team}"
   propagate_at_launch = true
 }
tag {
   key =  "SSMCWconfig"
   value = "${var.tag_ssmcwconfig}"
   propagate_at_launch = true
 }
}

#FOR MON-FRI
resource "aws_autoscaling_schedule" "start_1" {
  scheduled_action_name           = "${aws_autoscaling_group.app_asg.name}-1-asg-start-schedule"
  autoscaling_group_name          = "${aws_autoscaling_group.app_asg.name}"
  min_size                        = "${var.min_size}"
  max_size                        = "${var.max_size}"
  desired_capacity                = "${var.desired_capacity}"
  recurrence                      = "${var.asg_start_cron_expression_1}"
}

resource "aws_autoscaling_schedule" "stop_1" {
  scheduled_action_name           = "${aws_autoscaling_group.app_asg.name}-1-asg-stop-schedule"
  autoscaling_group_name          = "${aws_autoscaling_group.app_asg.name}"
  min_size                        = "${var.schedule_stop}"
  max_size                        = "${var.schedule_stop}"
  desired_capacity                = "${var.schedule_stop}"
  recurrence                      = "${var.asg_stop_cron_expression_1}"
}

#________Creation of Auto scaling policies - Sacle in and Scale out__________________________________
resource "aws_autoscaling_policy" "asp_cpu_scale_out" {
  name = "${aws_launch_configuration.app_lc.name}-Scale-Out for CPU"
  autoscaling_group_name = "${aws_autoscaling_group.app_asg.name}"
  adjustment_type = "ChangeInCapacity"
  policy_type = "SimpleScaling"
  scaling_adjustment = "1"
  cooldown = "300"
}

resource "aws_cloudwatch_metric_alarm" "cpu_scale_out" {
  alarm_name = "${var.app_stack_name}-${var.tag_environment}-Scale-Out for CPU-Utilization"
  alarm_description = "scale out ec2 on cpu utilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.asg_evaluation_periods_so}"
  metric_name         = "${var.asg_metric_name}"
  namespace           = "${var.asg_namespace}"
  period              = "${var.asg_period_so}"
  statistic           = "${var.asg_statistic}"
  threshold           = "${var.asg_threshold_so}"
  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.app_asg.name}"
  }
  actions_enabled = true
  alarm_actions = [
    "${aws_autoscaling_policy.asp_cpu_scale_out.arn}", "${var.sns_topic_alert}", "${var.sns_topic_info}"
	]
}

resource "aws_autoscaling_policy" "asp_cpu_scale_in" {
  name = "${aws_launch_configuration.app_lc.name}-Scale-In for CPU"
  autoscaling_group_name = "${aws_autoscaling_group.app_asg.name}"
  adjustment_type = "ChangeInCapacity"
  policy_type = "SimpleScaling"
  scaling_adjustment = "-1"
  cooldown = "300"
}

resource "aws_cloudwatch_metric_alarm" "cpu_scale_in" {
  alarm_name = "${var.app_stack_name}-${var.tag_environment}-Scale-In for CPU-Utilization"
  alarm_description = "scale in ec2 on cpu utilization"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "${var.asg_evaluation_periods_si}"
  metric_name         = "${var.asg_metric_name}"
  namespace           = "${var.asg_namespace}"
  period              = "${var.asg_period_si}"
  statistic           = "${var.asg_statistic}"
  threshold           = "${var.asg_threshold_si}"
  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.app_asg.name}"
  }
  actions_enabled = true
  alarm_actions = [
    "${aws_autoscaling_policy.asp_cpu_scale_in.arn}", "${var.sns_topic_alert}", "${var.sns_topic_info}"
	]
}

#_________________________________CloudWatchAlarms_______________________________________________
#---------------------------------CPU Utilization------------------------------------------------
resource "aws_cloudwatch_metric_alarm" "cpu_utilisation_info" {
  alarm_name = "${var.app_stack_name}-${var.tag_environment}-CPU-Utilization-Warning"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "${var.asg_namespace}"
  period = "${var.cpu_period}"
  statistic = "${var.asg_statistic}"
  threshold = "${var.cpu_threshold_info}"
  dimensions = {AutoScalingGroupName  = "${aws_autoscaling_group.app_asg.name}" }
  alarm_description = "${var.cpu_threshold_info}% cpu utilisation reached"
  alarm_actions = [
	 "${var.sns_topic_alert}", "${var.sns_topic_info}" 
  ]
}

resource "aws_cloudwatch_metric_alarm" "cpu_utilisation_action" {
  alarm_name = "${var.app_stack_name}-${var.tag_environment}-CPU-Utilization-Critical"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "${var.asg_namespace}"
  period = "${var.cpu_period}"
  statistic = "${var.asg_statistic}"
  threshold = "${var.cpu_threshold_action}"
  dimensions = {AutoScalingGroupName  = "${aws_autoscaling_group.app_asg.name}"  }
  alarm_description = "${var.cpu_threshold_action}% cpu utilisation reached, Alert."
  alarm_actions = [
    "${var.sns_topic_alert}", "${var.sns_topic_info}" 
  ]
}

resource "aws_cloudwatch_metric_alarm" "memory_utilisation_info" {
  alarm_name = "${var.app_stack_name}-${var.tag_environment}-Memory-utilization-Warning"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "Memory Utilization"
  namespace = "${var.asg_namespace1}"
  period = "${var.memory_period}"
  statistic = "Maximum"
  threshold = "${var.memory_threshold_info}"
  dimensions = {AutoScalingGroupName  = "${aws_autoscaling_group.app_asg.name}"  }
  alarm_description = "${var.memory_threshold_info} memory utilisation reached"
  alarm_actions = [
    "${var.sns_topic_alert}", "${var.sns_topic_info}"
  ]
}

resource "aws_cloudwatch_metric_alarm" "memory_utilisation_action" {
  alarm_name = "${var.app_stack_name}-${var.tag_environment}-Memory-utilization-Crtical"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "Memory Utilization"
  namespace = "${var.asg_namespace1}"
  period = "${var.memory_period}"
  statistic = "Maximum"
  threshold = "${var.memory_threshold_action}"
  dimensions = {AutoScalingGroupName  = "${aws_autoscaling_group.app_asg.name}" }
  alarm_description = "${var.memory_threshold_action} memory utilisation reached, Alert."
  alarm_actions = [
    "${var.sns_topic_alert}", "${var.sns_topic_info}"
  ]
}

#-----------------------------------Instance Status Check-----------------------------------------------------------
resource "aws_cloudwatch_metric_alarm" "status_check_failed" {
  alarm_name = "${var.app_stack_name}-${var.tag_environment}-statuscheckFailed-Crtical"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods = "2"
  metric_name = "StatusCheckFailed"
  namespace = "${var.asg_namespace}"
  period = "${var.inst_status_period}" 
  statistic = "Maximum"
  threshold = "0"
  dimensions = {AutoScalingGroupName  = "${aws_autoscaling_group.app_asg.name}"  }
  alarm_description = "System/Instance status failed"
  alarm_actions = [
    "${var.sns_topic_alert}", "${var.sns_topic_info}"
  ]
}
