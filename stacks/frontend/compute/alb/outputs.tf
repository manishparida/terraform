output "alb_id"                      {
	value = "${aws_alb.app_alb.id}"
}

output "alb_arn"                     {
	value = "${aws_alb.app_alb.arn}"
}

output "alb_dns_name"                {
	value = "${aws_alb.app_alb.dns_name}"
}

output "alb_zone_id"                 {
	value = "${aws_alb.app_alb.zone_id}"
}

output "tg_1_arn"                    {
	value = "${aws_alb_target_group.app_tg_http.arn}"
}

output "alb_listener_arn"            {	
	value = "${aws_lb_listener.app_alb_listener.arn}"
}

output "app_alb_listener_rule_http"  {	
	value = "${aws_alb_listener_rule.app_alb_listener_rule_http.arn}"
}