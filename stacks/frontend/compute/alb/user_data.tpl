<powershell>
Start-Transcript -Path C:\Windows\Temp\Computer.log
$logfile = "C:\Windows\Temp\userdata.log"
Write-Output "Adding User to Administrator"
cmd /c powershell -Command \"Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force\"
cmd /c net user /add "tempUser" "Qwerty@123#"
cmd /c net localgroup administrators "tempUser" /add

$IsInstalled = ((Get-WindowsFeature -Name Web-Server).installed)
if ($isinstalled -eq $false){
    Install-WindowsFeature Web-Server -IncludeManagementTools -IncludeAllSubFeature -Confirm:$false
    Write-Output "Installation Complete"
}
else{
    Write-output "IIS Server (WebServer) is already installed"
}

$url = "https://go.microsoft.com/fwlink/?LinkId=287166"
$output = "C:\temp\WebPlatformInstaller_amd64_en-US.msi"
$start_time = Get-Date
mkdir "C:\Temp"
Invoke-WebRequest -Uri $url -OutFile $output
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"
C:\temp\WebPlatformInstaller_amd64_en-US.msi /quiet

Remove-Item -Path C:\inetpub\wwwroot\iisstart.htm -Force
Remove-Item -Path C:\inetpub\wwwroot\iisstart.png -Force

[Net.ServicePointManager]::SecurityProtocol = "Tls12, Tls11, Tls, Ssl3"
Invoke-WebRequest "https://manishserver.jfrog.io/artifactory/example-repo-local/php-master.zip" -OutFile "C:\Temp\php-master.zip"
Expand-Archive -LiteralPath "C:\Temp\php-master.zip" -DestinationPath "C:\inetpub\wwwroot\"

Stop-Transcript
</powershell>
<persist>true</persist>