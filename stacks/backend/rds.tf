terraform {
    backend "s3" {}
}

provider "aws" {
    version         = "~> 2.7"
    region          = "${var.aws_region}"
    assume_role {
        role_arn = "arn:aws:iam::${var.aws_account}:role/${var.team_deployer_role}"
    }
}

data "aws_vpc" "client_vpc" {
    filter {
        name    = "tag:Name"
        values     = ["${var.vpc_name}"]
    }
    state         = "available"
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}


data "aws_subnet" "amber1" {
  filter {
    name   = "tag:Name"
    values = ["${var.amber_a}"]
  }
}

data "aws_subnet" "amber2" {
  filter {
    name   = "tag:Name"
    values = ["${var.amber_b}"]
  }
}
data "aws_subnet" "amber3" {
  filter {
    name   = "tag:Name"
    values = ["${var.amber_c}"]
  }
}

# RDS

resource "aws_db_instance" "rds" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  username             = "admin"
  password             = "admin123"
  parameter_group_name = "mysqldatabase"
  option_group_name     = "default:mysql-8-0"
  auto_minor_version_upgrade = "true"
  multi_az             = "false"
  port                 = "3306"
  vpc_security_group_ids = ["sg-13999b71"]
  publicly_accessible  = "true"

  tags = {
    Owner       = "manishparida123@hotmail.com"
    Name        = "mydb-rds"
    Environment = "dev"
  }
}