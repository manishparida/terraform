aws_account           			= "095972611389"
aws_management_account 			= "095972611389"
environment            			= "dev"
account_type           			= "nonprod"
vpc_name               			= "default-vpc"
state_prefix				        = "default-vpc/test"
amber_c                     = "amber-3"
amber_alb_3                 = "amber-3"

#=================Tags=====================
tag_environment        			= "dev"
tag_owner             			= "manishparida123@hotmail.com"

#=================EC2=====================
app_profile_name       			= "EC2_Role"
app_instance_type       		= "t2.micro"
app_stack_name			        = "test-web"
asg_az_count					      = "3"

#==============SNS==========================
sns_topics          			= [
  {
    name            			= "test-info"
    endpoint        			= "manishparida123@hotmail.com"
    protocol        			= "email"
  },
  {
    name            			= "test-alert"
    endpoint        			= "manishparida123@hotmail.com"
    protocol        			= "email"
  }
]
sns_topic_info            = "arn:aws:sns:ap-south-1:095972611389:test-info"  
sns_topic_alert           = "arn:aws:sns:ap-south-1:095972611389:test-alert"  

#==============Security Group==========================
mgmt_sg_name              = "windows"

#==================ALB===============
instance_port             = 80
lb_protocol               = "HTTP"
ssl_policy					      = "ELBSecurityPolicy-2016-08"
certificate_id          	= ""
	
min_size						      = "1"
max_size						      = "1"
desired_capacity				  = "1"
asg_start_cron_expression_1= "50 1 * * *"
asg_stop_cron_expression_1 = "0 16 * * *"
schedule_stop              = 0

#==============S3==========================
main_bucket_name            = "default-vpc-nonprod-test-backup"
main_bucket_acl             = "public"